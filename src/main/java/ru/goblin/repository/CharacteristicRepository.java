package ru.goblin.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.goblin.entity.Characteristic;

/**
 * Created by dolinskiy on 26.11.16.
 */
@Repository
public interface CharacteristicRepository extends CrudRepository<Characteristic, Long> {
}
