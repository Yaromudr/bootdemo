package ru.goblin.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.goblin.entity.User;

/**
 * Created by dolinskiy on 26.11.16.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
