package ru.goblin.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dolinskiy on 23.11.16.
 */
@Getter
@Setter
@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "foto_url")
    private String foto_url; //= "https://upload.wikimedia.org/wikipedia/commons/b/b0/NewTux.svg"

}
