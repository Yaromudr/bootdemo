package ru.goblin.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dolinskiy on 26.11.16.
 */
@Entity
@Getter
@Setter
@Table(name = "characteristics")
public class Characteristic implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
}
