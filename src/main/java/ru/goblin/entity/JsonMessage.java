package ru.goblin.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * Created by dolinskiy on 26.11.16.
 */
@Getter
@Setter
public class JsonMessage {
    private User user;
    private Set<Characteristic> characteristics;
}
