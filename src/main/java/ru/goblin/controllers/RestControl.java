package ru.goblin.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.goblin.entity.Characteristic;
import ru.goblin.entity.JsonMessage;
import ru.goblin.entity.User;
import ru.goblin.repository.CharacteristicRepository;
import ru.goblin.repository.UserRepository;

import java.util.HashSet;
import java.util.Set;

@RestController
public class RestControl {

    @Autowired
    CharacteristicRepository characteristicRepository;
    @Autowired
    UserRepository userRepository;

    @PostMapping(value = "/rest")
    public ResponseEntity generateNewPage() {
        long count = userRepository.count();
        long char_id;
        long user_id = (long) (Math.random() * count) + 1;
        User user = userRepository.findOne(user_id);

        count = characteristicRepository.count();
        Set<Characteristic> charSet = new HashSet<>(3);

        while (charSet.size() < 3) {
            char_id = (long) (Math.random() * count) + 1;
            charSet.add(characteristicRepository.findOne(char_id));
        }

        JsonMessage jsonMessage = new JsonMessage();
        jsonMessage.setUser(user);
        jsonMessage.setCharacteristics(charSet);


        return new ResponseEntity(jsonMessage, HttpStatus.OK);
    }

}